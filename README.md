# PowerCash SDK NodeJS (public)

Official NodeJS Wrapper for PowerCash API.

---

### How to

1. `npm install --save this_repo`

2. Instantiate module with options, example:

~~~
  var powercash = require('powercash-node');
  //Instantiate as a global, so you can use anywhere in your APP
  PowerCash = new powercash({type:'auth_type', name:'type_of_authorization', key:'your_key_or_token' }, { customerId: 'your_customer_id' });
~~~

3. Consume the API


~~~
  PowerCash.employee.list(function(err, data){
    if(err)
      throw err;

      console.log("PowerCash's response: ", data);
    })
~~~

---

# Available Methods & Contributing

Check the folder `lib/logic` for the available methods.

The architeture is simple, and new methods can be added as easy as 1, 2, 3:

1. Declare the method and its content, following the folder conventions
Remeber that the method's last parameter should be `cb, options`.

2. Make it available in powercash.js

3. Profit!

---

### Declaring:


### Folder Structure:

~~~
 - lib/logic/$ENTITY
 --- lib/logic/$ENTITY/index.js
~~~

### index.js file

`module.exports` a function that recieves `customerId` as parameter.
This function `returns` an object with the available methods.

~~~
var request = require('request');
var CFG = require('../../../config.json')

module.exports = function(customerId){
  return {
    method01: function(cb, options){
      return cb(error, data);
    },
    method02: function(object, param1, param2, cb, options){
      return cb(error, data);
    }

    [...]

  }
}

~~~

It is possible to make more modular, by `require()` each method:

~~~
 - lib/logic/$ENTITY
 --- lib/logic/$ENTITY/index.js
 -------lib/logic/$ENTITY/functions/$METHOD.js
~~~

- `index.js` will be

~~~
  return {
    method01: require('./functions/method01')
  }
~~~

- `lib/logic/$ENTITY/functions/$METHOD.js` will be

~~~
  module.exports = function(param01, param02, cb, options){

  };
~~~



## Making available


- `powercash.js` must have the following line, inside the exported function
~~~
this.$ENTITY = require('./logic/$ENTITY/index')(options.customerId);
~~~




## Profit

Now the new method can be used like

~~~
PowerCash.$ENTITY.$METHOD(param1, param2, function(err, data){}, options)
~~~
