var request = require('request');
var CFG = require('../../../config.json')

module.exports = function(customerId){
  return {
    create: function(contact, cb) {
      var postObj = {
        name      : contact.name,
        email     : contact.email,
        notes     : contact.notes,
        branch    : contact.branch,
        rejects   : contact.rejects,
        employee  : contact.employee,
        location  : contact.location,
        customer  : customerId,
        keyFields : contact.keyFields,
        keyFieldsSet : contact.keyFieldsSet,
        influence : contact.influence,
        occupation: contact.occupation
      }
      request.post(CFG.apiURL+'customers/'+customerId+'/contacts',{ headers: PowerCash.headers, json: postObj}, function (error, response, body) {
        return cb(error, body);
      })
    },
    search: function(query, cb){
      var url = CFG.apiURL+'customers/'+customerId+'/contacts/search?searching=true';

      if(query.name)
        url=url+'&name='+query.name;

      if(query.email)
        url=url+'&email='+query.email

      request.get({ url: url, headers: PowerCash.headers, json: true }, function (error, response, body) {
        return cb(error, body);
      })
    },
    get: function(id, cb){
      var url = CFG.apiURL+'customers/'+customerId+'/contacts/'+id;

      request.get({ url: url, headers: PowerCash.headers, json: true }, function (error, response, body) {
        return cb(error, body);
      })
    }

  }
}
