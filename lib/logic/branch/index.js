var request = require('request');
var CFG = require('../../../config.json')

module.exports = function(customerId){
  return {
    list: function(cb, options) {
      var url = CFG.apiURL+'customers/'+customerId+'/branches';

      request.get({ url: url, headers: PowerCash.headers }, function (error, response, body) {
        return cb(error, body);
      })
    },
    get: function(id, cb){
      var options = { url: CFG.apiURL+'customers/'+customerId+'/branches/'+id, headers: PowerCash.headers, json: true };
      request.get(options, function (error, response, body) {
        return cb(error, body);
      })
    },
    create: function(object, cb){
      var postObj = {
        name       : object.name,
        status     : object.status,
        type       : object.type,
        description: object.description
      };

      request.post({ url: CFG.apiURL+'/customers/'+customerId+'/branches', headers: PowerCash.headers }, postObj, function (error, response, body) {
        return cb(error, body);
      })
    },
    update: function(id, object, cb){
      var postObj = {};

        if (object.name)
          postObj.name = object.name;

        if (object.status)
          postObj.status = object.status;

        if (object.type)
          postObj.type = object.type;

        if (object.description)
          postObj.description = object.description;

      request.put({ url: CFG.apiURL+'/customers/'+customerId+'/branches/'+id, headers: PowerCash.headers }, postObj, function (error, response, body) {
        return cb(error, body);
      })
    },
    delete: function(id,cb){
      request.delete({ url: CFG.apiURL+'/customers/'+customerId+'/branches/'+id, headers: PowerCash.headers }, function (error, response, body) {
        return cb(error, body);
      })
    }

  }
};
