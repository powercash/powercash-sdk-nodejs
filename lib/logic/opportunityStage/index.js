var request = require('request');
var CFG = require('../../../config.json')

module.exports = function(customerId){
  return {
    get: function(opportunity, stage, cb, options){
      var url = CFG.apiURL+'customers/'+customerId+'/opportunities/'+opportunity+'/stages/'+stage;

      request.get({url: url, headers: PowerCash.headers, json: true }, function (error, response, body) {
        return cb(error, body);
      })
    },
    update: function(opportunity, stage, obj, cb, options){
      var url = CFG.apiURL+'customers/'+customerId+'/opportunities/'+opportunity+'/stages/'+stage;
      request({method: 'PUT', url: url, headers: PowerCash.headers, json: obj }, function (error, response, body) {
        return cb(error, body);
      })
    },
    setCurrentStage: function(opportunity, stage, obj, cb, options){

      var url = CFG.apiURL+'customers/'+customerId+'/opportunities/'+opportunity+'/stages/'+stage+'/current-stage';

      request.put({url: url, headers: PowerCash.headers, json: obj }, function (error, response, body) {
        return cb(error, body);
      })
    }
  }
}
