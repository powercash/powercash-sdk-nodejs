var request = require('request');
var CFG = require('../../../config.json')

module.exports = function(customerId){
  return {
    create: function(contact, cb) {
      console.log("contact creation ", contact);
      var postObj = {
        name      : contact.name,
        email     : contact.email,
        notes     : contact.notes,
        branch    : contact.branch,
        rejects   : contact.rejects,
        employee  : contact.employee,
        location  : contact.location,
        customer  : contact.customer,
        keyFields : contact.keyFields,
        influence : contact.influence,
        occupation: contact.occupation
      }
      request.post({ url: CFG.apiURL+'/customers/'+customerId+'/contacts', headers: PowerCash.headers, json: true }, postObj, function (error, response, body) {
        console.log("contact creation ", error);
        console.log("body ", body);
        return cb(error, body);
      })
    },
    search: function(query, cb){
      var url = CFG.apiURL+'/customers/'+customerId+'/contacts/search?searching=true';

      if(query.name)
        url=url+'&'+query.name;

      if(query.email)
        url=url+'&'+query.email

      request.get({ url: url, headers: PowerCash.headers, json: true }, function (error, response, body) {
        return cb(error, body);
      })
    }

  }
}
