var request = require('request');
var CFG = require('../../../config.json')

module.exports = function(customerId){
  return {
    create: function(obj, cb, options) {
      var url = CFG.apiURL+'customers/'+customerId+'/events';
      var newEvent = {
       due              : obj.due,
       type             : obj.type,
       name             : obj.name,
       body             : obj.body,
       branch           : obj.branch,
       priority         : obj.priority,
       category         : obj.category,
       contacts         : obj.contacts,
       customer         : obj.customerid,
       employee         : obj.employee,
       keyFields        : obj.keyFields,
       toSource         : obj.toSource,
       bodySet          : obj.bodySet,
       description      : obj.description,
       keyFieldsSet     : obj.keyFieldsSet,
       lead             : obj.lead,
       stage            : obj.stage,
       leadStage        : obj.leadStage,
       account          : obj.account,
       opportunity      : obj.opportunity,
       opportunityStage : obj.opportunityStage
      };

      request.post({url: url, headers: PowerCash.headers, json: newEvent }, function (error, response, body) {
        return cb(error, body);
      })
    },
    list: function(cb, options){
      var url = CFG.apiURL+'customers/'+customerId+'/events?searching=true';

      if(options.contact){
        url = url + '&contact='+options.contact;
      }
      if(options.status){
        url = url + '&status='+options.status.trim();
      }
      if(options.date){
        url = url + '&date='+options.date.trim();
      }
      if(options.due){
        url = url + '&due='+options.due.trim();
      }
      if(options.employee){
        url = url + '&employee='+options.employee.trim();
      }
      if(options.category){
        url = url + '&category='+options.category.trim();
      }
      if(options.name){
        url = url + '&name='+options.name.trim();
      }
      if(options.type){
        url = url + '&type='+options.type.trim();
      }
      if(options.priority){
        url = url + '&priority='+options.priority.trim();
      }
      if(options.toSource){
          url = url + '&toSource='+options.toSource.trim();
      }
      if(options.branch){
        url = url + '&branch='+options.branch.trim();
      }
      if(options.opportunity){
        url = url + '&opportunity='+options.opportunity.trim();
      }
      if(options.account){
        url = url + '&account='+options.account.trim();
      }
      if(options.opportunityStage){
        url = url + '&opportunityStage='+options.opportunityStage.trim();
      }
      if(options.leadStage){
        url = url + '&leadStage='+options.leadStage.trim();
      }
      if(options.lead){
        url = url + '&lead='+options.lead.trim();
      }

      request.get({url: url, headers: PowerCash.headers, json: true }, function (error, response, body) {
        return cb(error, body);
      })
    },
    do: function(id, obj, cb, options){
      var url = CFG.apiURL+'customers/'+customerId+'/events/'+id+'/do';
      if(obj && obj.justDo == 'true'){
        obj = {
          justDo: true
        }
      }
      request.put({url: url, headers: PowerCash.headers, json: obj }, function (error, response, body) {
        return cb(error, body);
      })
    }

  }
}
