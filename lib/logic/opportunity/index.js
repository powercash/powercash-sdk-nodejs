var request = require('request');
var CFG = require('../../../config.json')

module.exports = function(customerId){
  return {
    list: function(cb, options) {
      var url = CFG.apiURL+'customers/'+customerId+'/opportunities';
        if(options){

          url = url+'?searching=true';

          if(options.date){
            url = url+'&date='+options.date;
          }
          if(options.currentDate){
            url = url+'&currentDate='+options.currentDate;
          }
          if(options.employee){
            url = url+'&employee='+options.employee;
          }
          if(options.origin){
            url = url+'&origin='+options.origin;
          }
          if(options.reason){
            url = url+'&reason='+options.reason;
          }
          if(options.branch){
            url = url+'&branch='+options.branch;
          }
          if(options.status){
            url = url+'&status='+options.status;
          }
          if(options.slug){
            url = url+'&slug='+options.slug;
          }
          if(options.stage){
            url = url+'&stage='+options.stage;
          }
          if(options.accountActivity){
            url = url+'&accountActivity='+options.accountActivity;
          }
          if(options.agent){
            url = url+'&agent='+options.agent;
          }
          if(options.city){
            url = url+'&city='+options.city;
          }
          if(options.name){
            url = url+'&name='+options.name;
          }
          if(options.status){
            url = url+'&status='+options.status;
          }
          if(options.order){
            url = url+'&order='+options.order;
          }
        }

      request.get({ url: url, headers: PowerCash.headers, json: true }, function (error, response, body) {
        return cb(error, body);
      })
    },
    get: function(id, cb, options){
      var url = { url: CFG.apiURL+'customers/'+customerId+'/opportunities/'+id, headers: PowerCash.headers, json: true };
      request.get(url, function (error, response, body) {
        return cb(error, body);
      })
    },
    search: function(name, cb, options){
      var url = CFG.apiURL+'customers/'+customerId+'/opportunities/search?name='+name;

      request.get({url: url, headers: PowerCash.headers, json: true }, function (error, response, body) {
        return cb(error, body);
      })
    },
    create: function(obj, cb, options) {
      var newOpportunity = {
        customer       : customerId,
        name           : obj.name,
        status         : obj.status,
        branch         : obj.branch,
        contacts       : obj.contacts,
        keyFields      : obj.keyFields,
        initialStage   : obj.initialStage,
        lead           : obj.lead,
        origin         : obj.origin,
        account        : obj.account,
        employee       : obj.employee,
        location       : obj.location,
        probability    : obj.probability,
        products       : obj.product,
        keyFieldsSet   : obj.keyFieldsSet,
        primaryContact : obj.primaryContact
      }

      request.post(CFG.apiURL+'customers/'+customerId+'/opportunities',{ headers: PowerCash.headers, json: newOpportunity }, function (error, response, body) {
        return cb(error, body);
      })

  },
  update: function(_id, obj, cb, options) {
    var updatedOpportunity = obj;
    request.put(CFG.apiURL+'customers/'+customerId+'/opportunities/' + _id,{ headers: PowerCash.headers, json: updatedOpportunity }, function (error, response, body) {
      return cb(error, body);
    })

    },
    closed: function(_id, obj, cb, options) {
      var updatedOpportunity = obj;

      request.put(CFG.apiURL+'customers/'+customerId+'/opportunities/' + _id + '/closed',{ headers: PowerCash.headers, json: updatedOpportunity }, function (error, response, body) {
        return cb(error, body);
      })

    },
    setCompleteStage: function(_id, cb, options){
      request.put(CFG.apiURL+'customers/'+customerId+'/opportunities/' + _id + '/complete-stage',{ headers: PowerCash.headers, json: true}, function (error, response, body) {
        return cb(error, body);
      })

    }

  }
}
